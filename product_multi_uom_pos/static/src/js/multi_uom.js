odoo.define('product_multi_uom_pos.multi_uom',function(require) {
"use strict";

var gui = require('point_of_sale.gui');
var core = require('web.core');
var models = require('point_of_sale.models');
var OrderlineSuper = models.Orderline;
var pos_screens = require('point_of_sale.screens');

var _initialize_ = models.PosModel.prototype.initialize;
models.PosModel.prototype.initialize = function(session, attributes){
    self = this;
    for (var i = 0 ; i < this.models.length; i++){
        if (this.models[i].model == 'product.product'){
            if (this.models[i].fields.indexOf('product_pricelist_uom_ids') == -1) {
                this.models[i].fields.push('product_pricelist_uom_ids');
            }
        }
    }
    return _initialize_.call(this, session, attributes);
};

var posmodel_super = models.PosModel.prototype;
models.PosModel = models.PosModel.extend({
    initialize: function(session, attributes) {
        this.price_uom_by_id = {};
        posmodel_super.initialize.call(this,session, attributes);
    },
});


models.PosModel.prototype.models.push(
    {
        model: 'product.price.uom',
        fields: [],
        domain: null,
        loaded: function(self,price_uom){
            self.price_uom = price_uom;
            for(var i = 0; i < price_uom.length; i++){
                self.price_uom_by_id[price_uom[i].id] = price_uom[i];
            }
        }
    },
);

    models.Orderline = models.Orderline.extend({
        /*Adding uom_id to orderline*/
        initialize: function(attr,options){
            OrderlineSuper.prototype.initialize.call(this, attr, options);
            if (!options.json) {//evugor: solo si no se carga desde JSON
                this.uom_id = this ? this.product.uom_id: [];
            }
        },
        init_from_JSON: function(json) {//Evugor:tambien se inicia la UOM
            this.uom_id = json.uom_id || [];
            var res = OrderlineSuper.prototype.init_from_JSON.call(this, json);
            return res;
        },
        export_as_JSON: function() {
            var result = OrderlineSuper.prototype.export_as_JSON.call(this);
            result.uom_id = this.uom_id;
            return result;
        },
        /*this function now will return the uom_id of the orderline ,
        instead of the default uom_id of the product*/
        get_unit: function(){
            var res = OrderlineSuper.prototype.get_unit.call(this);
            var unit_id = this.uom_id;
            if(!unit_id){
                return res;
            }
            unit_id = unit_id[0];
            if(!this.pos){
                return undefined;
            }
            return this.pos.units_by_id[unit_id];
        },
        //evugor: Se pasa las funciones para el orderline para poder reutilizar
        /*function returns all the uom s in the specified category*/
        get_units_by_category: function(uom_list, categ_id){
            var uom_by_categ = []
            for (var uom in uom_list){
                if(uom_list[uom].category_id[0] == categ_id[0] && uom_list[uom].active){//EVUGOR: Filtra solo activos
                    uom_by_categ.push(uom_list[uom]);
                }
            }
            return uom_by_categ;
        },
        /*Find the base price(price of the product for reference unit)*/
        find_reference_unit_price: function(product, product_uom){
            if(product_uom.uom_type == 'reference'){
                return product.price;
            }
            else if(product_uom.uom_type == 'smaller'){
               return (product.price * product_uom.factor);
            }
            else if(product_uom.uom_type == 'bigger'){
               return (product.price / product_uom.factor_inv);
            }
        },
        /*finds the latest price for the product based on the new uom selected*/
        get_latest_price: function(uom, product){
            //busca el precio segun la uom
            if (product.product_pricelist_uom_ids){
                for(var i = 0; i < product.product_pricelist_uom_ids.length; i++){
                    var price_uom = this.pos.price_uom_by_id[product.product_pricelist_uom_ids[i]]
                    if (price_uom.uom_id[0] == uom.id){
                         return price_uom.price;
                    }
                }
            }
            var uom_by_category = this.get_units_by_category(this.pos.units_by_id, uom.category_id);
            var product_uom = this.pos.units_by_id[product.uom_id[0]];
            var ref_price = this.find_reference_unit_price(product, product_uom);
            var ref_unit = null;
            for (var i in uom_by_category){
                if(uom_by_category[i].uom_type == 'reference'){
                    ref_unit = uom_by_category[i];
                    break;
                }
            }
            if(ref_unit){
                if(uom.uom_type == 'bigger'){
                    return (ref_price * uom.factor_inv);
                }
                else if(uom.uom_type == 'smaller'){
                    return (ref_price / uom.factor);
                }
                else if(uom.uom_type == 'reference'){
                    return ref_price;
                }
            }
            return product.price;
        },
    });

    var exports = {};
    exports.set_uom_button = pos_screens.ActionButtonWidget.extend({
        template: 'SetUomButton',
        init: function (parent, options) {
            this._super(parent, options);

            this.pos.get('orders').bind('add remove change', function () {
                this.renderElement();
            }, this);

            this.pos.bind('change:selectedOrder', function () {
                this.renderElement();
            }, this);
        },

        /*function returns all the uom s in the specified category*/
//        get_units_by_category: function(uom_list, categ_id){
//            var uom_by_categ = []
//            for (var uom in uom_list){
//                if(uom_list[uom].category_id[0] == categ_id[0] && uom_list[uom].active){//EVUGOR: Filtra solo activos
//                    uom_by_categ.push(uom_list[uom]);
//                }
//            }
//            return uom_by_categ;
//        },
        /*Find the base price(price of the product for reference unit)*/
//        find_reference_unit_price: function(product, product_uom){
//            if(product_uom.uom_type == 'reference'){
//                return product.price;
//            }
//            else if(product_uom.uom_type == 'smaller'){
//               return (product.price * product_uom.factor);
//            }
//            else if(product_uom.uom_type == 'bigger'){
//               return (product.price / product_uom.factor_inv);
//            }
//        },
        /*finds the latest price for the product based on the new uom selected*/
//        get_latest_price: function(uom, product){
//            console.log("this", this)
//            console.log("uom", uom)
//            console.log("product", product)
//            console.log("product_pricelist_uom_ids", product.product_pricelist_uom_ids)
//            console.log("price_uom_by_id", this.pos.price_uom_by_id)
//            //busca el precio segun la uom
//            if (product.product_pricelist_uom_ids){
//                for(var i = 0; i < product.product_pricelist_uom_ids.length; i++){
//                    var price_uom = this.pos.price_uom_by_id[product.product_pricelist_uom_ids[i]]
//                    if (price_uom.uom_id[0] == uom.id){
//                         return price_uom.price;
//                    }
//                }
//            }
//            var uom_by_category = this.get_units_by_category(this.pos.units_by_id, uom.category_id);
//            console.log("price_uom_by_id", this.pos.price_uom_by_id)
//            var product_uom = this.pos.units_by_id[product.uom_id[0]];
//            var ref_price = this.find_reference_unit_price(product, product_uom);
//            var ref_unit = null;
//            for (var i in uom_by_category){
//                if(uom_by_category[i].uom_type == 'reference'){
//                    ref_unit = uom_by_category[i];
//                    break;
//                }
//            }
//            if(ref_unit){
//                if(uom.uom_type == 'bigger'){
//                    return (ref_price * uom.factor_inv);
//                }
//                else if(uom.uom_type == 'smaller'){
//                    return (ref_price / uom.factor);
//                }
//                else if(uom.uom_type == 'reference'){
//                    return ref_price;
//                }
//            }
//            return product.price;
//        },

        button_click: function () {
            var self = this;
            var orderline = this.pos.get_order().get_selected_orderline();
            var current_uom = this.pos.units_by_id[orderline.product.uom_id[0]];
            var uom_list = this.pos.units_by_id;
            var uom_by_category = orderline.get_units_by_category(uom_list, current_uom.category_id);
            var uomlist = _.map(uom_by_category, function (uom) {
                return {
                    label: uom.name,
                    item: uom
                };
            });

            self.gui.show_popup('selection', {
                title: 'Seleccione una Unidad de Medida',
                list: uomlist,
                confirm: function (uom) {
                    var order = self.pos.get_order();
                    var orderline = order.get_selected_orderline();
                    orderline.uom_id = [];
                    orderline.uom_id[0] = uom.id;
                    orderline.uom_id[1] = uom.display_name;
                    var latest_price = orderline.get_latest_price(uom, orderline.product);
                    orderline.set_unit_price(latest_price);
                    orderline.trigger('change',orderline);

                },
                is_selected: function (uom) {
                    return uom.id
                }
            });
        },

        get_current_pricelist_name: function () {
            var name = _t('Pricelist');
            var order = this.pos.get_order();

            if (order) {
                var pricelist = order.pricelist;

                if (pricelist) {
                    name = pricelist.display_name;
                }
            }
            return name;
        },
    });

    pos_screens.define_action_button({
        'name': 'set_uom_list',
        'widget': exports.set_uom_button,
        'condition': function () {
            return this.pos.config.sale_uom;
        },
    });
    return exports;

//
//var MultiUomWidget = PosBaseWidget.extend({
//    template: 'MultiUomWidget',
//    init: function(parent, args) {
//        this._super(parent, args);
//        this.options = {};
//        this.uom_list = [];
//    },
//    events: {
//        'click .button.cancel':  'click_cancel',
//        'click .button.confirm': 'click_confirm',
//    },
//    /*function returns all the uom s in the specified category*/
//    get_units_by_category: function(uom_list, categ_id){
//        var uom_by_categ = []
//        for (var uom in uom_list){
//            if(uom_list[uom].category_id[0] == categ_id[0]){
//                uom_by_categ.push(uom_list[uom]);
//            }
//        }
//        return uom_by_categ;
//    },
//    /*Find the base price(price of the product for reference unit)*/
//    find_reference_unit_price: function(product, product_uom){
//        if(product_uom.uom_type == 'reference'){
//            return product.price;
//        }
//        else if(product_uom.uom_type == 'smaller'){
//           return (product.price * product_uom.factor);
//        }
//        else if(product_uom.uom_type == 'bigger'){
//           return (product.price / product_uom.factor_inv);
//        }
//    },
//    /*finds the latest price for the product based on the new uom selected*/
//    get_latest_price: function(uom, product){
//        var uom_by_category = this.get_units_by_category(this.pos.units_by_id, uom.category_id);
//        var product_uom = this.pos.units_by_id[product.uom_id[0]];
//        var ref_price = this.find_reference_unit_price(product, product_uom);
//        var ref_unit = null;
//        for (var i in uom_by_category){
//            if(uom_by_category[i].uom_type == 'reference'){
//                ref_unit = uom_by_category[i];
//                break;
//            }
//        }
//        if(ref_unit){
//            if(uom.uom_type == 'bigger'){
//                return (ref_price * uom.factor_inv);
//            }
//            else if(uom.uom_type == 'smaller'){
//                return (ref_price / uom.factor);
//            }
//            else if(uom.uom_type == 'reference'){
//                return ref_price;
//            }
//        }
//        return product.price;
//    },
//    /*Rendering the wizard*/
//    show: function(options){
//        options = options || {};
//        console.log("EVUGOR:options", options)
//        var current_uom = this.pos.units_by_id[options.uom_list[0]];
//        console.log("EVUGOR:current_uom1111", current_uom)
//        var uom_list = this.pos.units_by_id;
//        var uom_by_category = this.get_units_by_category(uom_list, current_uom.category_id);
//        this.uom_list = uom_by_category;
//        this.current_uom = options.uom_list[0];
//        this.renderElement();
//    },
//    close: function(){
//        if (this.pos.barcode_reader) {
//            this.pos.barcode_reader.restore_callbacks();
//        }
//    },
//    click_confirm: function(){
//        var self = this;
//        var uom = parseInt(this.$('.uom').val());
//        console.log("EVUGOR:uom", uom)
//        var order = self.pos.get_order();
//        var orderline = order.get_selected_orderline();
//        var selected_uom = this.pos.units_by_id[uom];
//        console.log("EVUGOR:orderline", orderline)
//        orderline.uom_id = [];
//        orderline.uom_id[0] = uom;
//        orderline.uom_id[1] = selected_uom.display_name;
//
//        /*Updating the orderlines*/
//        order.remove_orderline(orderline);
//        order.add_orderline(orderline);
//        var latest_price = this.get_latest_price(selected_uom, orderline.product);
//        order.get_selected_orderline().set_unit_price(latest_price);
//        this.gui.close_popup();
//        return;
//
//    },
//    click_cancel: function(){
//        this.gui.close_popup();
//    },
//});
//gui.define_popup({name:'multi_uom_screen', widget: MultiUomWidget});
//
//pos_screens.ActionpadWidget.include({
//    /*opening the wizard on button click*/
//    renderElement: function() {
//        this._super();
//        var self = this;
//        this.$('.multi-uom-span').click(function(){
//            var orderline = self.pos.get_order().get_selected_orderline();
//            var options = {
//                'uom_list': orderline.product.uom_id
//            };
//            self.gui.show_popup('multi_uom_screen', options);
//        });
//    }
//});

});