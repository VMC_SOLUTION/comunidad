# -*- coding: utf-8 -*-

from odoo import api, fields, models

# class Pricelist(models.Model):
#     _inherit = "product.pricelist"

# class PricelistItem(models.Model):
#     _inherit = "product.pricelist.item"
#
#     min_quantity_uom = fields.Integer('Cantidad min', default=1)
#     pricelist_id = fields.Many2one(default=1)
#     uom_id = fields.Many2one('product.uom', string="UOM")
#
#     def _calc_uom(self):
#         """CALCULA LA CANTIDAD SEGUN UNIDAD DE MEDIDA SELECCIONADA"""
#         if self.uom_id:
#             if self.uom_id == self.product_tmpl_id.uom_id:
#                 self.min_quantity = self.min_quantity_uom
#             else:
#                 if self.uom_id.uom_type == 'bigger':
#                     self.min_quantity = self.min_quantity_uom * self.uom_id.factor_inv
#                 elif self.uom_id.uom_type == 'smaller':
#                     self.min_quantity = self.min_quantity_uom / self.uom_id.factor_inv
#                 else:
#                     self.min_quantity = self.min_quantity_uom
#         else:
#             self.min_quantity = self.min_quantity_uom
#
#     @api.onchange('min_quantity_uom')
#     def _onchange_min_quantity_uom(self):
#         self._calc_uom()
#
#     @api.onchange('uom_id')
#     def _onchange_uom(self):
#         self._calc_uom()
#         domain = {}
#         domain['uom_id'] = [('category_id', '=', self.product_tmpl_id.uom_id.category_id.id)]
#         return {'domain': domain}

class ProductPriceUOM(models.Model):
    _name = "product.price.uom"

    name = fields.Char('name')
    product_template_id = fields.Many2one('product.template', 'Item')
    uom_id = fields.Many2one('product.uom', string="Unid. Medida")
    price = fields.Float('Precio')

    @api.onchange('uom_id')
    def _onchange_uom(self):
        domain = {}
        if self._context.get('uom', False):
            domain['uom_id'] = [('category_id', '=', self.env['product.uom'].browse(self._context.get('uom')).category_id.id)]
            return {'domain': domain}

class ProductTemplate(models.Model):
    _inherit = "product.template"

    product_pricelist_uom_ids = fields.One2many('product.price.uom', 'product_template_id', 'Precio venta UOM')

