# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools, _
from odoo.exceptions import UserError

class PosConfig(models.Model):
    _inherit = "pos.config"

    sale_uom = fields.Boolean('Vender en diferentes UOM', default=False, help='Permite vender en diferentes unidades de medida')