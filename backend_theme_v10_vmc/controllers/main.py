# -*- coding: utf-8 -*-
# Copyright 2016, 2017 Openworx
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html).

import os
import base64
from odoo.http import Controller, request, route

from odoo.addons.backend_theme_v10.controllers.main import DasboardBackground

class DasboardBackground(DasboardBackground):

    @route(['/dashboard'], type='http', auth='user', website=False)
    def dashboard(self, **post):
        """Se sobrescribe porque al cargar la imagen direcciona a http y no a https"""
        path_root = os.path.join(os.path.dirname(os.path.abspath(__file__)))
        user = request.env.user
        company = user.company_id
        if company.dashboard_background:
            image = base64.b64decode(company.dashboard_background)
        else:
            image = open(path_root+'/D6B688B10.jpg', 'rb').read()
        return request.make_response(
            image, [('Content-Type', 'image')])