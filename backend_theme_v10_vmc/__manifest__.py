# -*- coding: utf-8 -*-
# Copyright 2016, 2017 Openworx
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html).

{
    "name": "Material Backend V10 por VMC",
    "summary": "Odoo 10.0 community backend theme por VMC",
    "version": "10.0.1.0.2",
    "category": "Themes/Backend",
    "website": "http://www.openworx.nl",
	"description": """
		Backend theme for Odoo 11.0 community edition.
    """,
	'images':[
        'images/screen.png'
	],
    "author": "Openworx",
    "license": "LGPL-3",
    "installable": True,
    "depends": [
        'backend_theme_v10',
    ],
    "data": [
        'views/assets.xml',
    ],
}

