# -*- coding: utf-8 -*-
# Copyright 2016, 2017 Openworx
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html).

from odoo import api, models, fields

class ResUsers(models.Model):

    _inherit = 'res.users'

    @api.model
    def update_sidebar(self):
        self._cr.execute(
            """
            update  res_users set sidebar_visible=false
            """
        )