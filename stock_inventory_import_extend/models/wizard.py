# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models

class wizard_stock_inventory_import_force(models.TransientModel):
    _name = 'wizard.stock.inventory.import.force'

    inventory_id = fields.Many2one('stock.inventory')

    def process(self):
        print('inventory_id', self.inventory_id)
        self.inventory_id.with_context(force=True).action_done()