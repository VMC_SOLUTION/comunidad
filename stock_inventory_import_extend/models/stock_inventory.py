 # -*- coding: utf-8 -*-
# (c) 2015 Oihane Crucelaegui - AvanzOSC
# License AGPL-3 - See http://www.gnu.org/licenses/agpl-3.0.html

from odoo import fields, api, models, _
from odoo.exceptions import UserError

class StockInventory(models.Model):
    _inherit = "stock.inventory"

    user_id = fields.Many2one('res.users', string="Responsable", readonly=True, states={'draft': [('readonly', False)], 'confirm': [('readonly', False)]})
    fecha_toma = fields.Datetime('Fecha de conteo', readonly=True, states={'draft': [('readonly', False)], 'confirm': [('readonly', False)]}, default=fields.Datetime.now, help='Fecha que se realizó el conteo de los productos')
    inventory_general = fields.Boolean('Inventario General', readonly=True, states={'draft': [('readonly', False)]}, help='Marcar para incluir los Productos con Stock que no esten presentes en el archivo y al validar el inventario ponerlo en Cero')


    @api.multi
    def process_import_lines(self):
        """Procesa nuevamente los productos no encontrados y los busca por nombre del producto"""
        super(StockInventory, self).process_import_lines()
        import_lines = self.mapped('import_lines')#Hacia abajo la misma logica
        inventory_line_obj = self.env['stock.inventory.line']
        product_obj = self.env['product.product']
        for line in import_lines:
            if line.fail:
                if not line.product:
                    prod_lst = product_obj.search([('name', '=', line.code.strip()), ('active', '=', True)])
                    if prod_lst and len(prod_lst) == 1:#Solo debe existir uno
                        product = prod_lst[0]
                    elif prod_lst and len(prod_lst) > 1:
                        line.fail_reason = 'Existe dos productos con el mismo Nombre'
                        continue
                    else:
                        line.fail_reason = _('No product code found')
                        continue
                else:
                    product = line.product
                lot_id = None
                line_inventory = inventory_line_obj.search(
                    [('product_id', '=', product.id), ('inventory_id', '=', line.inventory_id.id)])
                if line_inventory:#EVUGOR
                    line_inventory.product_qty += line.quantity
                else:
                    inventory_line_obj.create({
                        'product_id': product.id,
                        'product_uom_id': product.uom_id.id,
                        'product_qty': line.quantity,
                        'inventory_id': line.inventory_id.id,
                        'location_id': line.location_id.id,
                        'prod_lot_id': lot_id})
                line.write({'fail': False, 'fail_reason': _('Processed')})
        if self.inventory_general:
            self.process_no_file()
        return True


    @api.multi
    def action_done(self):
        for inventory in self:
            for iml in inventory.import_lines:
                if iml.fail and not self._context.get('force', False):
                    view = self.env.ref('stock_inventory_import_extend.view_wizard_stock_inventory_import_force')
                    wiz = self.env['wizard.stock.inventory.import.force'].create({'inventory_id': inventory.id})
                    return {
                        'name': 'Verifique',
                        'type': 'ir.actions.act_window',
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'wizard.stock.inventory.import.force',
                        'views': [(view.id, 'form')],
                        'view_id': view.id,
                        'target': 'new',
                        'res_id': wiz.id,
                    }
        return super(StockInventory, self).action_done()

    def process_no_file(self):
        """Agrega los productos no presentes en el Archivo"""
        inventory_line_obj = self.env['stock.inventory.line']
        product_obj = self.env['product.product']
        self._cr.execute("""
            select  product_id, sum(stock_quant.qty)
            from    stock_quant
            where   location_id = (%s)
            group by product_id
            Having sum(stock_quant.qty)<>0
        """, (self.location_id.id,))
        products_stock = self._cr.fetchall()
        for stock in products_stock:
            line_inventory = inventory_line_obj.search([('product_id', '=', stock[0]),
                                                        ('inventory_id', '=', self.id)
                                                        ])
            if not line_inventory:
                product = product_obj.browse(stock[0])
                inventory_line_obj.create({
                    'product_id': product.id,
                    'no_file': True,
                    'product_uom_id': product.uom_id.id,
                    'product_qty': 0,
                    'inventory_id': self.id,
                    'location_id': self.location_id.id,
                    'prod_lot_id': None})

    def download_detalle_inventory(self):
        import openpyxl
        from openpyxl.styles import Font
        import tempfile
        import os
        import base64
        file_tmp = tempfile.NamedTemporaryFile(suffix='.xlsx')
        wb = openpyxl.workbook.Workbook()

        #Lineas importadas
        worksheet = wb.create_sheet(index=0, title='Lineas importadas')
        data_cab = self.read(['date', 'location_id'])
        headers = ['Producto', 'Codigo', 'Cantidad', 'Estado']
        for x in range(1, len(headers) + 1):
            worksheet.cell(row=1, column=x).font = Font(bold=True)
            worksheet.cell(row=1, column=x).value = headers[x - 1]
        row = 2
        for line in self.import_lines:
            worksheet.cell(row=row, column=1).value = line.product.display_name if line.product else ''
            worksheet.cell(row=row, column=2).number_format = '@'
            worksheet.cell(row=row, column=2).value = str(line.code)
            worksheet.cell(row=row, column=3).value = line.quantity
            worksheet.cell(row=row, column=4).value = line.fail_reason
            row += 1
        #Detalle del inventario
        worksheet2 = wb.create_sheet(index=0, title='Detalle del inventario')
        headers = ['Producto', 'Cant. teorica', 'Cant. real']
        for x in range(1, len(headers) + 1):
            worksheet2.cell(row=1, column=x).font = Font(bold=True)
            worksheet2.cell(row=1, column=x).value = headers[x - 1]
        row = 2
        for line in self.line_ids:
            worksheet2.cell(row=row, column=1).value = line.product_id.display_name if line.product_id else ''
            worksheet2.cell(row=row, column=2).value = line.theoretical_qty
            worksheet2.cell(row=row, column=3).value = line.product_qty
            row += 1
        wb.save(file_tmp.name)
        file_xls = open(file_tmp.name, 'rb')
        fields = {
            'name': 'Inventario {}'.format(self.location_id.display_name),
            'datas': base64.b64encode(file_xls.read()),
            'datas_fname': 'Inventario {}'.format(self.location_id.display_name),
            'type': 'binary',
        }
        attach = self.env['ir.attachment'].create(fields)
        file_xls.close()
        os.remove(file_tmp.name)
        return {
            'type': 'ir.actions.act_url',
            'url': '/web/content/%s?download=true' % attach.id,
            'target': 'new',
        }

class InventoryLine(models.Model):
    _inherit = "stock.inventory.line"

    no_file = fields.Boolean('No presente en Archivo', default=False)

class ImportInventory(models.TransientModel):
    _inherit = 'import.inventory'

    @api.one
    def _convert_csv_to_xls(self):
        import os
        import tempfile
        import base64
        import pandas as pd

        #Crea temporales
        fxls, xls_fname = tempfile.mkstemp()
        fcsv, csv_fname = tempfile.mkstemp(suffix='.csv')
        #Excribe los datos del archivo Excel
        os.write(fxls, base64.b64decode(self.data))
        os.close(fxls)
        #Converte de Excel a CSV
        pd.read_excel(xls_fname).to_csv(csv_fname, encoding="utf-8", index=False)
        #Lee el archivo CSV
        file_csv = open(csv_fname, 'r+')
        data_csv = base64.b64encode(file_csv.read())
        self.data = data_csv
        #Elimina temporales
        os.unlink(xls_fname)
        os.unlink(csv_fname)

    def action_import(self):
        """Si es un archivo XLS lo convierte en CSV"""
        self._convert_csv_to_xls()
        #No considera la fecha en el nombre del inventario y mantiene el nombre dado inicialmente al inventario y añade el nombre del archivo
        ctx = self._context
        inventory_obj = self.env['stock.inventory']
        if 'active_id' in ctx:
            inventory = inventory_obj.browse(ctx['active_id'])
        inv_name = inventory.name + ': ' + self.name
        result = super(ImportInventory, self).action_import()
        if inventory:
            inventory.write({'name': inv_name})
        return result