# -*- coding: utf-8 -*-
from odoo import fields, api, models, _
from odoo.exceptions import Warning
import base64
import csv
import cStringIO


class ImportInventory(models.TransientModel):
    _name = 'import.inventory'
    _description = 'Import inventory'

    def _get_default_location(self):
        ctx = self._context
        if 'active_id' in ctx:
            inventory_obj = self.env['stock.inventory']
            inventory = inventory_obj.browse(ctx['active_id'])
        return inventory.location_id

    data = fields.Binary('File', required=True)
    name = fields.Char('Filename')
    delimeter = fields.Char('Delimeter', default=',',
                            help='Default delimeter is ","')
    location = fields.Many2one('stock.location', 'Default Location',
                               default=_get_default_location, required=True)

    def action_import(self):
        """Load Inventory data from the CSV file."""
        ctx = self._context
        print('context action_import', self._context)
        stloc_obj = self.env['stock.location']
        inventory_obj = self.env['stock.inventory']
        inv_imporline_obj = self.env['stock.inventory.import.line']
        product_obj = self.env['product.product']
        if 'active_id' in ctx:
            inventory = inventory_obj.browse(ctx['active_id'])
        if not self.data:
            raise Warning(_("You need to select a file!"))
        # Decode the file data
        data = base64.b64decode(self.data)
        file_input = cStringIO.StringIO(data)
        file_input.seek(0)
        location = self.location
        reader_info = []
        if self.delimeter:
            delimeter = str(self.delimeter)
        else:
            delimeter = ','
        reader = csv.reader(file_input, delimiter=delimeter,
                            lineterminator='\r\n')
        print('data padree', self.data)
        try:
            reader_info.extend(reader)
        except Exception:
            raise Warning(_("Not a valid file!"))
        keys = reader_info[0]
        # check if keys exist
        if not isinstance(keys, list) or ('code' not in keys or
                                          'quantity' not in keys):
            raise Warning(
                _("Not 'code' or 'quantity' keys found"))
        del reader_info[0]
        values = {}
        actual_date = fields.Date.today()
        inv_name = self.name + ' - ' + actual_date
        inventory.write({'name': inv_name,
                         'date': fields.Datetime.now(),
                         'imported': True, 'state': 'confirm'})
        for i in range(len(reader_info)):
            val = {}
            field = reader_info[i]
            values = dict(zip(keys, field))
            prod_location = location.id
            if 'location' in values and values['location']:
                locations = stloc_obj.search([('name', '=',
                                               values['location'])])
                prod_location = locations[:1].id
            prod_lst = product_obj.search(['|', '|', ('default_code', '=', values['code']), ('default_code', '=', values['code'].upper()), ('default_code', '=', values['code'].lower())])
            if not prod_lst:
                prod_lst = product_obj.search(['|', '|', ('barcode', '=', values['code']), ('barcode', '=', values['code'].upper()), ('barcode', '=', values['code'].lower())])
            if prod_lst:
                val['product'] = prod_lst[0].id
            # #Lunitex, Busca solo el color
            # if self.env.user.company_id.vat == '20603279477' and not prod_lst:
            #     #Si el peso esta incluido en el codigo
            #     if len(values['code']) >= 12:
            #         extrae = len(values['code']) - 5
            #         codigo = values['code'][:extrae]
            #         prod_lst = product_obj.search([('default_code', '=', codigo)])
            #         if prod_lst:
            #             val['product'] = prod_lst[0].id
            #             peso = values['code'][-5:]
            #             quantity = float(peso[:3]+'.'+peso[-2:])
            #             values['quantity'] = quantity
            #     if not prod_lst:
            #         codigo = values['code'][:7]
            #         prod_lst = product_obj.search([('default_code', '=', codigo)])
            #         if prod_lst:
            #             val['product'] = prod_lst[0].id
            #         #busca el peso en la serie
            #         self._cr.execute("""
            #             select 	sum(peso)
            #             from	serie
            #             where	codigo=%s
            #         """, (values['code'], ))
            #         result = self._cr.fetchone()
            #         if result:
            #             values['quantity'] = result[0]
            # #fin Lunitex
            if 'lot' in values and values['lot']:
                val['lot'] = values['lot']
            val['code'] = values['code']
            val['quantity'] = values['quantity']
            val['location_id'] = prod_location
            val['inventory_id'] = inventory.id
            val['fail'] = True
            val['fail_reason'] = _('No processed')
            val['name_file'] = self.name[:29]
            self._create_line_import(val)

    def _create_line_import(self, values):
        inv_imporline_obj = self.env['stock.inventory.import.line']
        inv_imporline_obj.create(values)


class StockInventoryImportLine(models.Model):
    _name = "stock.inventory.import.line"
    _description = "Stock Inventory Import Line"

    code = fields.Char('Product Code')
    product = fields.Many2one('product.product', 'Found Product')
    quantity = fields.Float('Quantity')
    inventory_id = fields.Many2one('stock.inventory', 'Inventory',
                                   readonly=True)
    location_id = fields.Many2one('stock.location', 'Location')
    lot = fields.Char('Product Lot')
    fail = fields.Boolean('Fail', default=True)
    fail_reason = fields.Char('Fail Reason')
    name_file = fields.Char('Nombre archivo', size=30, help='Nombre del archivo cargado')
