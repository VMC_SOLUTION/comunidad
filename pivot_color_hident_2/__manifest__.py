# -*- coding: utf-8 -*-
{
    'name': 'Color ident 2 pivot',
    'version': '10.0.1.0.0',
    'summary': 'Color ident 2 pivot',
    'category': 'Reporting',
    'author': 'VMCLOUD SOLUTION',
    'depends': ['web'],
    'data': [
        'views/templates.xml',
    ],
    'license': 'LGPL-3',
    'installable': True,
    'auto_install': False,
    'application': False,
}
