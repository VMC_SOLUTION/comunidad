odoo.define('pivot_heat_map.PivotView', function (require) {
    "use strict";

    var PivotView = require('web.PivotView');
    var core = require('web.core');
    var formats = require('web.formats');

    var _t = core._t;
    PivotView.include({
//        get_bg_color: function(id_row){
//            var R = id_row/(Math.sqrt(256));
//            var G = (id_row/256) % 256;
//            var B = id_row%256;
//            var val_rgb = 'rgb('+R+','+G+','+B+')'
//            console.log("rgb", id_row, '=>', val_rgb)
//            if (R>255 || R < 0){
//                console.log("RED INVALIDOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO")
//            }
//            if (G>255 || G < 0){
//                console.log("GREEN INVALIDOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO")
//            }
//            if (B>255 || B < 0){
//                console.log("BLUE INVALIDOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO")
//            }
//            return val_rgb;
//        },
        draw_rows: function ($tbody, rows) {
            //Sobrescribe para poder inclluir color por fila al primer nivel de ident, solo si tiene mas de un nivel de
            var apply_color = false;
            for (var i = 0; i < rows.length; i++) {
                if (rows[i].indent > 1) {
                    apply_color = true;
                    break;
                }
            }
            if (!apply_color){
                return this._super($tbody, rows)
            }
            var self = this,
            i, j, value, $row, $cell, $header,
            nbr_measures = this.active_measures.length,
            length = rows[0].values.length,
            display_total = this.main_col.width > 1;
            var groupby_labels = _.map(this.main_row.groupbys, function (gb) {
                return self.fields[gb.split(':')[0]].string;
            });
            var measure_types = this.active_measures.map(function (name) {
                return self.measures[name].type;
            });
            var widgets = this.widgets;
            for (i = 0; i < rows.length; i++) {
                $row = $('<tr>');
                $header = $('<td>')
                    .text(rows[i].title)
                    .data('id', rows[i].id)
                    .css('padding-left', (5 + rows[i].indent * 30) + 'px')
                    .addClass(rows[i].expanded ? 'o_pivot_header_cell_opened' : 'o_pivot_header_cell_closed');
                if (rows[i].indent > 0) $header.attr('title', groupby_labels[rows[i].indent - 1]);
                if (rows[i].indent == 1){
                    //this.get_bg_color(rows[i].id)
                    $header.css('background-color', '#d2dffb');
                }
                $header.appendTo($row);
                for (j = 0; j < length; j++) {
                    value = formats.format_value(rows[i].values[j], {type: measure_types[j % nbr_measures], widget: widgets[j % nbr_measures]});
                    $cell = $('<td>')
                                .data('id', rows[i].id)
                                .data('col_id', rows[i].col_ids[Math.floor(j / nbr_measures)])
                                .toggleClass('o_empty', !value)
                                .text(value)
                                .addClass('o_pivot_cell_value text-right');
                    if (rows[i].indent == 1) $cell.css('background-color', '#d2dffb');
                    if (((j >= length - this.active_measures.length) && display_total) || i === 0){
                        $cell.css('font-weight', 'bold');
                    }
                    $row.append($cell);

                    $cell.toggleClass('hidden-xs', j < length - this.active_measures.length);
                }
                $tbody.append($row);
            }
        },
    });
});
